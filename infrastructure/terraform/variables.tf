# Credentials are read in through terraform.tfvars (not in git)
variable "access_key" {}

variable "secret_key" {}

# AWS Region
variable "region" {
  default = "us-east-1"
}

# VPC name to use
variable "main-vpc" {
type = "map"
default = {
  "cidr-block" = "10.84.0.0/16"
  "tag" = "HERE Drupal VPC"
  }
}


# App Subnet A CIDR Block
variable "AppA-CIDR" {
default = "10.84.0.0/24" 
}

# App Subnet B CIDR Block
variable "AppB-CIDR" {
default = "10.84.1.0/24"
}

# The AMI image to use (Ubuntu 18)
variable "aws_amis" {
  default = {
    "us-east-1" = "ami-00aa0e472da3198b7"
    "us-east-2" = "ami-00aa0e472da3198b7"
  }
}

# RDS DB variable
variable "rds-vars" {
type = "map"
default = {
  "tag" = " HERE GCO KM RDS"
  }
}

# Size of the instance
variable "instance_type" {
  default = "t3.micro"
}

# SSH key to deploy
variable "key_name" {
  description = "Key pair to use"
  default     = "HERE-TerraForm-Key"
}

# Whitelist your IP for SSH access here
variable "ip_whitelist" {
  description = "Whitelisted IP for SSH access"
  default     = "0.0.0.0/0"
}
