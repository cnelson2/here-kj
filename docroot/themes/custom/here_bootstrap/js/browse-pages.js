+function ($) {
  'use strict';

  // When the user scrolls the page, execute myFunction
  window.onscroll = function() {myFunction()};

  // Get the filters
  let filters = $('div[class*=" view-browse-"] .view-filters.form-group');

  // Get the offset position of the navbar
  let sticky = filters.offset();

  // Add the sticky class to the filters when you reach its scroll position. Remove "sticky" when you leave the scroll position
  function myFunction() {
    if(sticky) {
      if (window.pageYOffset > sticky.top - 100) {
        filters.addClass('sticky');
      } else {
        filters.removeClass('sticky');
      }
    }
  }

}(jQuery);
