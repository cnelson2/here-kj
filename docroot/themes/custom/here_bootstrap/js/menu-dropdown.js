+function ($) {
  'use strict';

  // Commented out until we can figure out why bootstrap dropdown JS in conflicting with the mobile menu
  // function setMenuDropdownActions(mediaQuery) {
    
  //   if (mediaQuery.matches) { // mobile
  //     $('.menu--main .expanded.dropdown .dropdown-toggle').removeClass('disabled');
  //     $('.menu--main .expanded.dropdown').click(function() {
  //       $(this).addClass('open');
  //     }, function() {
  //       $(this).removeClass('open');
  //     });

  //   } else { // desktop
  //     $('.menu--main .expanded.dropdown .dropdown-toggle').addClass('disabled');
  //     $('.menu--main .expanded.dropdown').hover(function() {
  //         $(this).addClass('open');
  //     }, function() {
  //       $(this).removeClass('open');
  //     });
  //   }

  // }
  
  // var mobileMediaQuery = window.matchMedia("(max-width: 768px)")
  // setMenuDropdownActions(mobileMediaQuery) // Call listener function at run time
  // mobileMediaQuery.addListener(setMenuDropdownActions) // Attach listener function on state changes


  // This will do for now
  $('.menu--main .expanded.dropdown .dropdown-toggle').addClass('disabled');
  $('.menu--main .expanded.dropdown').hover(function() {
      $(this).addClass('open');
  }, function() {
    $(this).removeClass('open');
  });

  $(document).on('click', 'button[value=Search]', function (item) {
    utag.link({ link_id:$(this),link_text:'hereKM:Search',linkEvent:'hereKM:Search',actionTrack:'hereKM:Search',searchTerm:'api'});
  });

  $(document).on('click', 'a', function (item) {
    let text = $(this).text().trim();
    text = text.replace(/\s+/g, '-').toLowerCase();

    if(typeof utag !== 'undefined') {
      utag.link({ link_id:$(this), link_text: 'hereKM:' + text, linkEvent: 'hereKM:' + text, actionTrack: 'hereKM:' + text});



      // utag.link({
      //   link_id:$(this),
      //   link_text:'hereKM:download:sdk_latest_version',
      //   linkEvent:'hereKM:download:sdk_latest_version',
      //   actionTrack:'hereKM:download:sdk_latest_version'
      // });
    }
  });





}(jQuery);
