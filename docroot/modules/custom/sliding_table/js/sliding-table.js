/**
 * @file
 * Javascript functionality for Display Suite's administration UI.
 */

(function ($, Drupal, drupalSettings) {
  let initialized;

  Drupal.behaviors.customers = {
    attach: function(context) {
      let $context = $(context);

      if (document.readyState == 'loading') {
        // loading yet, wait for the event
        document.addEventListener('DOMContentLoaded', function() {
          domContentLoaded($context);
        });
      } else {
        // DOM is ready!
        domContentLoaded($context);
      }
    }
  };

  function domContentLoaded($context) {
    init($context);

    if ( $.fn.dataTable.isDataTable( '#deliveryModelTable' ) ) {
      let table = $('#deliveryModelTable').DataTable();
    } else {

      $('#deliveryModelTable thead tr').clone(true).appendTo( '#deliveryModelTable thead' );

      let table = $('#deliveryModelTable').DataTable( {
        data: drupalSettings.table_data,
        deferRender: true,
        paging: true,
        ordering: true,
        pagingType: 'full_numbers',
        orderCellsTop: true,
        lengthMenu: [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]]
      } );

      $('#deliveryModelTable thead tr:eq(1) th').each( function (i) {
        let title = $(this).text();
        let option = [];

        option.push('<option value="">Filter '+title+'</option>');
        table.column(i).data().unique().each( function(item) {
          let html = $.parseHTML(item);

          if($(html).find( 'li' ).length > 0) {
            $(html).find( 'li' ).each( function(i, list_item) {
              let list_text = $(list_item).text();
              if(list_text) {
                option.push('<option value="'+list_text+'">'+list_text+'</option>');
              }
            });
          } else {
            let text = $(html).text();
            if(text) {
              option.push('<option value="'+text+'">'+text+'</option>');
            }
          }

        });

        option = $.unique(option.sort());

        let select = $('<select style="width: 100%">'+option.join('')+'</select>');
        $(this).html( select );

        $( 'select', this ).on( 'change', function () {
          if ( table.column(i).search() !== this.value ) {
            table
              .column(i)
              .search( this.value )
              .draw();
          }
        } );
      } );
    }

  }

  function init($context) {
    if (!initialized) {
      initialized = true;
      // Add your one-time only code here
      $('#conditionalTab').on('click', function (e) {
        if($('#productDeliveryConditional').is(':empty')) {
          setConditionalTable($context);
          $("[data-toggle=tooltip]").tooltip();
          $context.find('#productDeliveryConditional').parent().append('<div id="life-cycle"></div>');
        }
      });
    }
  }

  function setConditionalTable($context) {
    let data = drupalSettings.conditional_table;
    let hover_definitions = drupalSettings.hover_definitions;

    $context.find('#productDeliveryConditional').append('<div id="deliveryListGroup" class="list-group">' +
      '<div class="list-group-item list-group-item-heading" data-toggle="tooltip" data-placement="top" title="'+ hover_definitions['DELIVERY_MODEL_TOOLTIP'] +'">Delivery Model</div>\n' +
      '<div class="list-group-container"></div>\n' +
      '</div>');

    $.each(data, function( index, value ) {

      if(value.alias) {
        $('<a href="#" class="list-group-item list-group-item-action" title="'+index+'" alias="'+value.alias+'">'+ index +'</a>\n').appendTo('#deliveryListGroup .list-group-container');
        delete value.alias;
      } else {
        $('<a href="#" class="list-group-item list-group-item-action" title="'+index+'">'+ index +'</a>\n').appendTo('#deliveryListGroup .list-group-container');
      }

    });

    $context.find('#productDeliveryConditional').append('<div id="vendorListGroup" class="list-group">' +
      '<div class="list-group-item list-group-item-heading" data-toggle="tooltip" data-placement="top" title="'+ hover_definitions['SYSTEM_VENDOR_TOOLTIP'] +'">System Vendors</div>\n' +
      '<div class="list-group-container"></div>\n' +
      '</div>');

    $context.find('#productDeliveryConditional').append('<div id="navListGroup" class="list-group">' +
      '<div class="list-group-item list-group-item-heading" data-toggle="tooltip" data-placement="top" title="'+ hover_definitions['NAVIGATION_PLATFORM_TOOLTIP'] +'">Navigation Platform</div>\n' +
      '<div class="list-group-container"></div>\n' +
      '</div>');

    $context.find('#productDeliveryConditional').append('<div id="customerProductsListGroup" class="list-group">' +
      '<div class="list-group-item list-group-item-heading" data-toggle="tooltip" data-placement="top" title="'+ hover_definitions['CUSTOMER_PRODUCT_TOOLTIP'] +'">Products</div>\n' +
      '<div class="list-group-container"></div>\n' +
      '</div>');

    $context.find('#productDeliveryConditional').append('<div id="hereProductsListGroup" class="list-group">' +
      '<div class="list-group-item list-group-item-heading" data-toggle="tooltip" data-placement="top" title="'+ hover_definitions['HERE_PRODUCT_TOOLTIP'] +'">HERE Products</div>\n' +
      '<div class="list-group-container"></div>\n' +
      '</div>');

    $context.find('#productDeliveryConditional').append('<div id="componentsListGroup" class="list-group">' +
      '<div class="list-group-item list-group-item-heading" data-toggle="tooltip" data-placement="top" title="'+ hover_definitions['COMPONENT_TOOLTIP'] +'">Components</div>\n' +
      '<div class="list-group-container"></div>\n' +
      '</div>');

    $context.find('#productDeliveryConditional').append('<div id="programsListGroup" class="list-group">' +
      '<div class="list-group-item list-group-item-heading" data-toggle="tooltip" data-placement="top" title="'+ hover_definitions['PROGRAM_TOOLTIP'] +'">Programs</div>\n' +
      '<div class="list-group-container"></div>\n' +
      '</div>');

    $context.find('#productDeliveryConditional').append('<div id="businessListGroup" class="list-group">' +
      '<div class="list-group-item list-group-item-heading" data-toggle="tooltip" data-placement="top" title="'+ hover_definitions['BUSINESS_TOOLTIP'] +'">Business Groups</div>\n' +
      '<div class="list-group-container"></div>\n' +
      '</div>');

    $context.find('#productDeliveryConditional').append('<div id="subBusinessListGroup" class="list-group">' +
      '<div class="list-group-item list-group-item-heading" data-toggle="tooltip" data-placement="top" title="'+ hover_definitions['SUB_BUSINESS_TOOLTIP'] +'">Sub-Business Groups</div>\n' +
      '<div class="list-group-container"></div>\n' +
      '</div>');


    setClickEvents($context, data);
  }

  function setClickEvents($context, data) {

    // Delivery Models
    $context.find('#deliveryListGroup .list-group-container .list-group-item').on('click', function (e) {
      e.preventDefault();
      $(this).siblings().removeClass('active');
      $(this).addClass('active');

      $('#deliveryListGroup').nextAll().each( function( index, element ){
        $( this ).find('.list-group-container').empty();
      });

      let deliveryListGroup = data[$(this).text()];

      $.each(deliveryListGroup, function( index, value ) {
        $('<a href="#" class="list-group-item list-group-item-action" title="'+index+'">'+ index +'</a>\n').appendTo('#vendorListGroup .list-group-container');
      });


      // System Vendors
      $context.find('#vendorListGroup .list-group-container .list-group-item').on('click', function (e) {
        e.preventDefault();
        $(this).siblings().removeClass('active');
        $(this).addClass('active');

        $('#vendorListGroup').nextAll().each( function( index, element ){
          $( this ).find('.list-group-container').empty();
        });

        let vendorListGroup = deliveryListGroup[$(this).text()];

        $.each(vendorListGroup, function( index, value ) {
          $('<a href="#" class="list-group-item list-group-item-action" title="'+index+'">'+ index +'</a>\n').appendTo('#navListGroup .list-group-container');
        });


        // Navigation Platforms
        $context.find('#navListGroup .list-group-container .list-group-item').on('click', function (e) {
          e.preventDefault();
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          $('#navListGroup').nextAll().each( function( index, element ){
            $( this ).find('.list-group-container').empty();
          });

          let navListGroup = vendorListGroup[$(this).text()];

          $.each(navListGroup, function( index, value ) {
            if(value.alias) {
              $('<a href="#" class="list-group-item list-group-item-action" title="'+index+'" alias="'+value.alias+'">'+ index +'</a>\n').appendTo('#customerProductsListGroup .list-group-container');
              delete value.alias;
            } else {
              $('<a href="#" class="list-group-item list-group-item-action" title="'+index+'">'+ index +'</a>\n').appendTo('#customerProductsListGroup .list-group-container');
            }
          });


          // Customer Products
          $context.find('#customerProductsListGroup .list-group-container .list-group-item').on('click', function (e) {
            e.preventDefault();
            $(this).siblings().removeClass('active');
            $(this).addClass('active');

            $('#customerProductsListGroup').nextAll().each( function( index, element ){
              $( this ).find('.list-group-container').empty();
            });

            let customerProductsListGroup = navListGroup[$(this).text()];

            $.each(customerProductsListGroup, function( index, value ) {
              if(value.alias) {
                $('<a href="#" class="list-group-item list-group-item-action" title="'+index+'" alias="'+value.alias+'">'+ index +'</a>\n').appendTo('#hereProductsListGroup .list-group-container');
                delete value.alias;
              } else {
                $('<a href="#" class="list-group-item list-group-item-action" title="'+index+'">'+ index +'</a>\n').appendTo('#hereProductsListGroup .list-group-container');
              }
            });


            // Here Products
            $context.find('#hereProductsListGroup .list-group-container .list-group-item').on('click', function (e) {
              e.preventDefault();
              $(this).siblings().removeClass('active');
              $(this).addClass('active');

              $('#hereProductsListGroup').nextAll().each( function( index, element ){
                $( this ).find('.list-group-container').empty();
              });

              let hereProductsListGroup = customerProductsListGroup[$(this).text()];

              $.each(hereProductsListGroup, function( index, value ) {
                $('<a href="#" class="list-group-item list-group-item-action" title="'+index+'">'+ index +'</a>\n').appendTo('#componentsListGroup .list-group-container');
              });


              // Components
              $context.find('#componentsListGroup .list-group-container .list-group-item').on('click', function (e) {
                e.preventDefault();
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                $('#componentsListGroup').nextAll().each( function( index, element ){
                  $( this ).find('.list-group-container').empty();
                });

                let componentsListGroup = hereProductsListGroup[$(this).text()];

                $.each(componentsListGroup['program_name'], function( index, value ) {
                  if(value.alias) {
                    $('<a href="#" class="list-group-item list-group-item-action" title="'+value.name+'" alias="'+value.alias+'">'+ value.name +'</a>\n').appendTo('#programsListGroup .list-group-container');
                  } else {
                    $('<a href="#" class="list-group-item list-group-item-action" title="'+value.name+'">'+ value.name +'</a>\n').appendTo('#programsListGroup .list-group-container');
                  }
                });


                // Programs
                $context.find('#programsListGroup .list-group-container .list-group-item').on('click', function (e) {
                  e.preventDefault();
                  $(this).siblings().removeClass('active');
                  $(this).addClass('active');

                  $('#programsListGroup').nextAll().each( function( index, element ){
                    $( this ).find('.list-group-container').empty();
                  });

                  $.each(componentsListGroup['business_list'], function( index, value ) {
                    $('<a href="#" class="list-group-item list-group-item-action" title="'+value+'">'+ value +'</a>\n').appendTo('#businessListGroup .list-group-container');
                  });

                  // Business Groups
                  $context.find('#businessListGroup .list-group-container .list-group-item').on('click', function (e) {
                    e.preventDefault();
                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');

                    $('#businessListGroup').nextAll().each( function( index, element ){
                      $( this ).find('.list-group-container').empty();
                    });

                    $.each(componentsListGroup['sub_business_list'], function( index, value ) {
                      $('<a href="#" class="list-group-item list-group-item-action" title="'+value+'">'+ value +'</a>\n').appendTo('#subBusinessListGroup .list-group-container');
                    });


                    // Sub-Business Groups
                    $context.find('#subBusinessListGroup .list-group-container .list-group-item').on('click', function (e) {
                      e.preventDefault();
                      $(this).siblings().removeClass('active');
                      $(this).addClass('active');

                      $('#life-cycle').empty();
                      $('<h3>LIFE CYCLE</h3>').appendTo('#life-cycle');
                      $('<div id="lifeCycleContainer"></div>').appendTo('#life-cycle');

                      $('#productDeliveryConditional').children().each( function( index, element ){
                        let title = $( this ).find('.active').text();
                        let alias = $( this ).find('.active').attr('alias');

                        if(alias) {
                          $('<a href="'+alias+'" class="list-group-item list-group-item-action" title="'+title+'">'+ title +'</a>\n').appendTo('#life-cycle #lifeCycleContainer');
                        } else {
                          $('<div class="list-group-item list-group-item-action" title="'+title+'">'+ title +'</div>\n').appendTo('#life-cycle #lifeCycleContainer');
                        }

                      });

                      $('html, body').animate({
                        scrollTop: $('#life-cycle').offset().top - $("#navbar").outerHeight(true)
                      }, 700);
                    });
                  });
                });
              });
            });
          });
        });
      });
    });



  }
})(jQuery, Drupal, drupalSettings);


