<?php

namespace Drupal\sliding_table\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use \ReflectionClass;


/**
 * Provides a 'Table' Block.
 *
 * @Block(
 *   id = "table_block",
 *   admin_label = @Translation("Table Block"),
 *   category = @Translation("Table Block"),
 * )
 */
class TableBlock extends BlockBase {

  const DELIVERY_MODEL_TOOLTIP = 'Way of delivering HERE data to the customer';
  const SYSTEM_VENDOR_TOOLTIP = 'External supplier how converts HERE data into OEM compilation';
  const NAVIGATION_PLATFORM_TOOLTIP = 'Type of hardware on which the navigation runs';
  const CUSTOMER_PRODUCT_TOOLTIP = 'End product to the customer';
  const HERE_PRODUCT_TOOLTIP = 'Level 2 product HERE data';
  const COMPONENT_TOOLTIP = 'Level 3 product HERE data';
  const PROGRAM_TOOLTIP = 'A group of projects worked upon to create content for a specific product';
  const BUSINESS_TOOLTIP = 'High Level Business groups within HERE';
  const SUB_BUSINESS_TOOLTIP = 'Content Creation Groups within GCO';

  /**
   * {@inheritdoc}
   */

  public function build() {
    $conditional_table = [];
    $table = '';
    if (!empty($node = \Drupal::routeMatch()->getParameter('node')) && is_object($node)) {

      $whole_table = $this->getTableData($node->id());
      $table = $this->formatTableJSON($whole_table);
      $conditional_table = $this->formatConditionalData($whole_table);
    }

    $reflection = new ReflectionClass(TableBlock::class);
    $constants = $reflection->getConstants();

    return [
      '#theme' => 'here_template_key',
      '#hover_definitions' => $constants,
      '#attached' => [
        'drupalSettings' => [
          'table_data' => $table,
          'conditional_table' => $conditional_table,
          'hover_definitions' => $constants
        ],
        'library' => [
          'sliding_table/sliding-table'
        ],
      ],
    ];
  }

  private function getTableData($nid) {

    $query = \ Drupal :: database () -> select ('node_field_data', 'nfd');
    $query-> addField ('tfd_3', 'name', 'delivery_model');
    $query-> addField ('tfd_3', 'tid', 'del_tid');
    $query-> addField ('url_1', 'alias', 'del_alias');

    $query-> addField ('tfd_1', 'name', 'system_vendor');
    $query-> addField ('tfd_1', 'tid', 'sys_tid');
    $query-> addField ('url_2', 'alias', 'sys_alias');


    $query-> addField ('tfd_2', 'name', 'navigation_platform');
    $query-> addField ('tfd_2', 'tid', 'nav_tid');
    $query-> addField ('url_4', 'alias', 'nav_alias');


    $query-> addField ('fd_2', 'title', 'customer_product');
    $query-> addField ('cp', 'field_customer_product_target_id', 'cp_nid');
    $query-> addField ('url_3', 'alias', 'cp_alias');


    $query-> addField ('prb', 'field_product_bundle_ids_value', 'bundle_ids');

    $query-> leftjoin ('node__field_system_vendor_product_bund', 'bund', 'nfd.nid = bund.entity_id');

    $query-> leftjoin ('paragraph__field_system_vendor', 'sv', 'bund.field_system_vendor_product_bund_target_id = sv.entity_id');
    $query-> leftjoin('taxonomy_term_field_data', 'tfd_1', 'tfd_1.tid = sv.field_system_vendor_target_id');
    $query-> leftjoin('url_alias', 'url_2', 'CONCAT( \'/taxonomy/term/\', tfd_1.tid ) = url_2.source');


    $query-> leftjoin ('paragraph__field_navigation_platform', 'np', 'bund.field_system_vendor_product_bund_target_id = np.entity_id');
    $query-> leftjoin('taxonomy_term_field_data', 'tfd_2', 'tfd_2.tid = np.field_navigation_platform_target_id');
    $query-> leftjoin('url_alias', 'url_4', 'CONCAT( \'/taxonomy/term/\', tfd_2.tid ) = url_4.source');


    $query-> leftjoin ('paragraph__field_customer_product', 'cp', 'bund.field_system_vendor_product_bund_target_id = cp.entity_id');
    $query-> leftjoin('node_field_data', 'fd_2', 'cp.field_customer_product_target_id = fd_2.nid');
    $query-> leftjoin('url_alias', 'url_3', 'CONCAT( \'/node/\', fd_2.nid ) = url_3.source');


    $query-> leftjoin ('node__field_delivery_model', 'dm', 'dm.entity_id = cp.field_customer_product_target_id');
    $query-> leftjoin('taxonomy_term_field_data', 'tfd_3', 'tfd_3.tid = dm.field_delivery_model_target_id');
    $query-> leftjoin('url_alias', 'url_1', 'CONCAT( \'/taxonomy/term/\', tfd_3.tid ) = url_1.source');


    $query-> leftjoin ('node__field_product_bundle_ids', 'prb', 'prb.entity_id = cp.field_customer_product_target_id');


    $query-> condition ('nfd.nid', $nid);

    $system_vendors = $query-> execute() -> fetchAll ();

    return $system_vendors;
  }

  private function formatTableJSON($data) {

    $table = [];
    foreach ($data as $row) {
      $bundle_ids = $row->bundle_ids;
      $cp_id = substr($bundle_ids, 0, strpos($bundle_ids, ':'));
      $here_product = \Drupal\node\Entity\Node::load($cp_id);
      $here_product_name = $here_product ? $here_product->getTitle() : '';
      $here_product_alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$cp_id);


      $id_str = substr($bundle_ids, strpos($bundle_ids, ':') + 1);
      $ids = explode(',', $id_str);

      for ($i = 0; $i < count($ids); $i++) {
        $element = $ids[$i];
        $p = \Drupal\paragraphs\Entity\Paragraph::load( $element );

        $component_name = '';
        if($p && $p->field_component) {
          $tid = $p->field_component->target_id;
          $term = \Drupal\taxonomy\Entity\Term::load($tid);
          $component_name = $term ? $term->getName() : '';
        }


        $program_name = '';
        if($p && $p->field_product_programs) {
          $program_name = '<ul>';
          $tids = $p->field_product_programs->getValue();
          foreach($tids as $tid) {
            $term = \Drupal\taxonomy\Entity\Term::load($tid['target_id']);
            if ($term) {

              // Run query
              $alias = $this->getProgramAlias($tid['target_id']);
              $program_name .= $alias ? '<li><a href="'. $alias .'">' . $term->getName() . '</a></li>' : '<li>' . $term->getName() . '</li>';
            }
          }
          $program_name .= '</ul>';
        }

        $business_list = '';
        if($p && $p->field_business_groups) {
          $business_list = '<ul>';
          $tids = $p->field_business_groups->getValue();
          foreach($tids as $tid) {
            $term = \Drupal\taxonomy\Entity\Term::load($tid['target_id']);
            if ($term) {
              $business_list .= '<li>' . $term->getName() . '</li>';
            }
          }
          $business_list .= '</ul>';
        }

        $sub_business_list = '';
        if($p && $p->field_business_sub_groups) {
          $sub_business_list = '<ul>';
          $tids = $p->field_business_sub_groups->getValue();
          foreach($tids as $tid) {
            $term = \Drupal\taxonomy\Entity\Term::load($tid['target_id']);
            if($term) {
              $sub_business_list .= '<li>' . $term->getName() . '</li>';
            }
          }
          $sub_business_list .= '</ul>';
        }

        $delivery_model_row = $row->del_alias ? '<a href="'. $row->del_alias .'">' . $row->delivery_model . '</a>' : $row->delivery_model;
        $customer_product_row = $row->cp_alias ? '<a href="'. $row->cp_alias .'">' . $row->customer_product . '</a>' : $row->customer_product;
        $here_product_row = $here_product_alias ? '<a href="'. $here_product_alias .'">' . $here_product_name . '</a>' : $here_product_name;

        array_push($table, [
          $delivery_model_row,
          $row->system_vendor,
          $row->navigation_platform,
          $customer_product_row,
          $here_product_row,
          $component_name,
          $program_name,
          $business_list,
          $sub_business_list
        ]);
      }
    }

    return $table;
  }

  private function formatConditionalData($data) {

    $delivery_models = array_column($data, 'delivery_model');
    $delivery_models = array_unique($delivery_models);

    $final_data = array();
    foreach ($delivery_models as $del_model) {

      $system_vendors = array_filter(
        $data,
        function($value) use ($del_model) {
          $result = $value->delivery_model == $del_model;
          return $result;
        }
      );

      $del_alias = array_column($system_vendors, 'del_alias');
      $del_alias = array_unique($del_alias);

      $system_vendors = array_column($system_vendors, 'system_vendor');
      $system_vendors = array_unique($system_vendors);

      foreach ($system_vendors as $vend) {
        $navigation_platforms= array_filter(
          $data,
          function($value) use ($vend, $del_model) {
            $result = ($value->system_vendor == $vend) && ($value->delivery_model == $del_model);
            return $result;
          }
        );
        $navigation_platforms = array_column($navigation_platforms, 'navigation_platform');
        $navigation_platforms = array_unique($navigation_platforms);


        foreach ($navigation_platforms as $nav) {
          $customer_products = array_filter(
            $data,
            function($value) use ($nav, $vend, $del_model) {
              $result = ($value->navigation_platform == $nav) && ($value->system_vendor == $vend) && ($value->delivery_model == $del_model);
              return $result;
            }
          );
          $customer_products = array_column($customer_products, 'customer_product');
          $customer_products = array_unique($customer_products);

          foreach ($customer_products as $prod) {
            $bundle_ids_strings = array_filter(
              $data,
              function($value) use ($prod, $nav, $vend, $del_model) {
                $result = ($value->customer_product == $prod) && ($value->navigation_platform == $nav) && ($value->system_vendor == $vend) && ($value->delivery_model == $del_model);
                return $result;
              }
            );

            $cp_alias = array_column($bundle_ids_strings, 'cp_alias');
            $cp_alias = array_unique($cp_alias);

            $bundle_ids_strings = array_column($bundle_ids_strings, 'bundle_ids');
            $bundle_ids_strings = array_unique($bundle_ids_strings);

            // Need to break down the bundle ids now and get the Here Product Name, Component, Program, Business Group and Sub-Business Group
            foreach ($bundle_ids_strings as $bundle_str) {

              $cp_id = substr($bundle_str, 0, strpos($bundle_str, ':'));
              $customer_product = \Drupal\node\Entity\Node::load($cp_id);
              $here_product_alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$cp_id);
              $customer_product_name = $customer_product ? $customer_product->getTitle() : '';

              $id_str = substr($bundle_str, strpos($bundle_str, ':') + 1);
              $pids = explode(',', $id_str);

              foreach ($pids as $pid) {
                $p = \Drupal\paragraphs\Entity\Paragraph::load($pid);

                $component_name = '';
                if ($p && $p->field_component) {
                  $tid = $p->field_component->target_id;
                  $term = \Drupal\taxonomy\Entity\Term::load($tid);
                  $component_name = $term ? $term->getName() : '';
                }

                $program_name = [];
                if($p && $p->field_product_programs) {
                  $tids = $p->field_product_programs->getValue();
                  foreach($tids as $tid) {
                    $term = \Drupal\taxonomy\Entity\Term::load($tid['target_id']);
                    $prog_alias = $this->getProgramAlias($tid['target_id']);

                    if ($term) {
                      $temp_prog = [];
                      $temp_prog['alias'] = $prog_alias ? $prog_alias : NULL;
                      $temp_prog['name'] = $term->getName();
                      array_push($program_name, $temp_prog);
                    }
                  }
                }

                $business_list = [];
                if($p && $p->field_business_groups) {
                  $tids = $p->field_business_groups->getValue();
                  foreach($tids as $tid) {
                    $term = \Drupal\taxonomy\Entity\Term::load($tid['target_id']);
                    if($term) {
                      array_push($business_list, $term->getName());
                    }
                  }
                }

                $sub_business_list = [];
                if($p && $p->field_business_sub_groups) {
                  $tids = $p->field_business_sub_groups->getValue();
                  foreach($tids as $tid) {
                    $term = \Drupal\taxonomy\Entity\Term::load($tid['target_id']);
                    if($term) {
                      array_push($sub_business_list, $term->getName());
                    }
                  }
                }

                $bundle_arr = array(
                  'component_name' => $component_name,
                  'program_name' => $program_name,
                  'business_list' => $business_list,
                  'sub_business_list' => $sub_business_list,
                  );


                $final_data[$del_model]['alias'] = $del_alias[0];
                $final_data[$del_model][$vend ? $vend : 'N/A'][$nav ? $nav : 'N/A'][$prod ? $prod : 'N/A']['alias'] = $cp_alias[0];
                $final_data[$del_model][$vend ? $vend : 'N/A'][$nav ? $nav : 'N/A'][$prod ? $prod : 'N/A'][$customer_product_name]['alias'] = $here_product_alias;
                $final_data[$del_model][$vend ? $vend : 'N/A'][$nav ? $nav : 'N/A'][$prod ? $prod : 'N/A'][$customer_product_name][$component_name] = $bundle_arr;
              }
            }
          }
        }
      }
    }

    return $final_data;
  }

  private function getProgramAlias($tid) {
    $query = \ Drupal :: database () -> select ('node_field_data', 'nfd');
    $query-> addField ('url', 'alias', 'alias');
    $query-> join ('node__field_program_name', 'prog', 'nfd.nid = prog.entity_id');
    $query-> leftjoin('url_alias', 'url', 'CONCAT( \'/node/\', nfd.nid ) = url.source');
    $query-> condition ('prog.field_program_name_target_id', $tid);
    $alias = $query-> execute() -> fetchField();

    return $alias ? $alias : NULL;
  }

  public function getCacheTags() {
    //With this when your node change your block will rebuild
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      //if there is node add its cachetag
      return Cache::mergeTags(parent::getCacheTags(), array('node:' . $node->id()));
    } else {
      //Return default tags instead.
      return parent::getCacheTags();
    }
  }

  public function getCacheContexts() {
    //if you depends on \Drupal::routeMatch()
    //you must set context of this block with 'route' context tag.
    //Every new route this block will rebuild
    return Cache::mergeContexts(parent::getCacheContexts(), array('route'));
  }

}


