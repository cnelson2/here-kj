/**
 * @file
 * Javascript functionality for the datatables within Tools Profile Input and Outputs section.
 */

(function ($, Drupal, drupalSettings) {

  let initialized;
  let io_data = drupalSettings.io_table;
  let img = drupalSettings.in_img;
  let filter_name = drupalSettings.filter_name;
  let filter_match = drupalSettings.filter_match;

  Drupal.behaviors.tools = {
    attach: function(context) {
      if (document.readyState == 'loading') {
        document.addEventListener('DOMContentLoaded', function () {
          init();
        });
      } else {
        init();
      }
    }
  };

  function init() {
    if (!initialized) {
      initialized = true;
      populateTable($, io_data, img, filter_name, filter_match);

      $('#imageModalCenter').on('show.bs.modal', function (evt) {
        console.log(evt);
        let modal = $(this);

        let org_img = evt.relatedTarget;
        let img_lrg = org_img.getAttribute('data-lrg-src');

        modal.find('.modal-header h5').empty();
        modal.find('.modal-header h5').append(org_img.getAttribute('data-filename'));

        modal.find('.modal-body').empty();
        modal.find('.modal-body').append('<img src="'+ img_lrg +'"/>');
      });
    }
  }

  function populateTable($, io_data) {
    $('#io_table').DataTable( {
      data: io_data,
      fixedHeader: true,
      autoWidth: true,

      columns: [
        {
          data: 'filter_name',
          defaultContent: 'No project/workstream available',
          title: 'Project/Workstream'
        },
        {
          data: 'in_text',
          defaultContent: 'No available Input Format',
          title: 'Input',
          render: function(data, type, row) {
            if (row.img && row.img_lrg) {
              return '<div id="input_fields"><img type="button" class="image-button" data-toggle="modal" data-filename="'+row.filename+'" data-target="#imageModalCenter" data-lrg-src="'+row.img_lrg+'" src="'+row.img+'"/><span>'+data+'</span></div>';
            } else {
              return '<div id="input_fields"><span>Input Format Image Unavailable</span><span>'+data+'</span></div>';
            }
          }
        },
        {
          data: 'db_text',
          defaultContent: 'No available Database',
          title: 'Database'
        },
        {
          data: 'out_text',
          defaultContent: 'No available Output Format',
          title: 'Output'
        }
      ],


      initComplete: function () {
        this.api().columns(0).every( function () {
          let column = this;

          // Set default selection in dropdown, search remaining column values, and apply filtering to each field item
          let select = $('<select id="dropdown" class="custom-select form-control"><option value="">Filter Projects/Workstreams</option></select>')
            .appendTo($('#io_table_filter'))
            .on( 'change', function () {
              let val = $.fn.dataTable.util.escapeRegex(
                $(this).val()
              );
              column
                .search( val ? '^'+val+'$' : '', true, false )
                .draw();
            });
          column.data().unique().sort().each( function ( d, j ) {
            if (d != null) {
              select.append('<option value="' + d + '">' + d + '</option>') // Avoid null filter items
            }
          });
        });
      },
    });
  }
}) (jQuery, Drupal, drupalSettings);
