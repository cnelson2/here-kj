<?php

namespace Drupal\io_blocks\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\media\Entity\Media;
use Drupal\image\Entity\ImageStyle;

/**
 * Provides an input/output table for Tools Profile.
 *
 * @Block(
 *     id = "io_block",
 *     admin_label = @Translation("IO Block"),
 *     category = @Translation("IO Block"),
 * )
 */
class IOBlock extends BlockBase {
  public function build() {
    $io_table = [];
    $filter_match = [];
    $filter_name = [];

    if (!empty($node = \Drupal::routeMatch()->getParameter('node')) && is_object($node)) {
      $nid = $node->id();
      $bundle = $node->bundle();
      $io_table = $this -> query($nid, $bundle, $node);
      if($io_table) {
        for ($i = 0; $i < count($io_table); $i++) {
          if($io_table[$i]->{'img_vid'}) {
            $media = Media::load($io_table[$i]->{'img_vid'});
            if ($media) {
              $io_table[$i]->{'img'} = ImageStyle::load('thumbnail')->buildUrl($media->get('image')->entity->uri->value);
              $io_table[$i]->{'filename'} = $media->get('image')->entity->filename->value;
              $io_table[$i]->{'img_lrg'} = ImageStyle::load('large')->buildUrl($media->get('image')->entity->uri->value);
            }
          }
        }
        if ($io_table[$i]->{'filter_name'} && $io_table[$i]->{'filter_id'}) {
          $filter_match[$io_table[$i]->{'filter_name'}] = $io_table[$i]->{'filter_id'};
          $filter_name[$i] = $io_table[$i]->{'filter_name'};
        }
      }
      $filter_name = array_unique($filter_name);
    }

    return [
      '#theme' => 'here_io_template_key',
      '#attached' => [
        'library' => [
          'io_blocks/io-blocks'
        ],
        'drupalSettings' => [
          'io_table' => $io_table,
          'filter_match' => $filter_match,
          'filter_name' => $filter_name
        ],
      ]
    ];
  }

  private function query($nid, $bundle, $node) {
    $query = \ Drupal :: database () -> select ('node__field_inputs_outputs', 'io_bund');

    $query -> addField('in_form', 'field_input_formats_value' ,'in_text');
    $query -> addField('img_id', 'vid', 'img_vid');
    $query -> addField('db', 'field_database_value', 'db_text');
    $query -> addField('out_form', 'field_output_formats_value', 'out_text');
    $query -> addField('pw_name', 'name', 'filter_name');
    $query -> addField('in_img', 'entity_id', 'img_id');
    $query -> addField('pw_filter', 'entity_id', 'filter_id');

    $query -> leftJoin('paragraphs_item_field_data', 'bund', 'bund.id = io_bund.field_inputs_outputs_target_id');
    $query -> leftJoin('paragraph__field_input_image', 'in_img', 'bund.id = in_img.entity_id');
    $query -> leftJoin('paragraph__field_input_formats' ,'in_form', 'bund.id = in_form.entity_id');
    $query -> leftJoin('paragraph__field_database' ,'db', 'bund.id = db.entity_id');
    $query -> leftJoin('paragraph__field_output_formats' ,'out_form', 'bund.id = out_form.entity_id');

    $query -> leftJoin('paragraph__field_projects_workstreams', 'pw_filter', 'bund.id = pw_filter.entity_id');
    $query -> leftJoin('taxonomy_term_field_data', 'pw_name', 'pw_filter.field_projects_workstreams_target_id = pw_name.tid');
    $query -> leftJoin('media_field_data', 'img_id', 'img_id.thumbnail__target_id = in_img.field_input_image_target_id');

    $query -> condition('io_bund.entity_id', $nid);
    $query -> orderBy('io_bund.entity_id');
    $results = $query -> execute() -> fetchAll();

    return $results;
  }

  public function getCacheTags() {
    //With this when your node change your block will rebuild
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      //if there is node add its cachetag
      return Cache::mergeTags(parent::getCacheTags(), array('node:' . $node->id()));
    } else {
      //Return default tags instead.
      return parent::getCacheTags();
    }
  }

  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), array('route'));
  }
}
