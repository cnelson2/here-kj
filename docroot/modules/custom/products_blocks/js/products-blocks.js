/**
 * @file
 * Javascript functionality for Display Suite's administration UI.
 */

(function ($, Drupal, drupalSettings) {
  let pagination;
  let initialized;
  let list = drupalSettings.list;
  let program_list = drupalSettings.program_list;
  let product_list = drupalSettings.product_list;
  let hover_definitions = drupalSettings.hover_definitions;

  Drupal.behaviors.products = {
    attach: function(context) {

      if (document.readyState == 'loading') {
        // loading yet, wait for the event
        document.addEventListener('DOMContentLoaded', function() {
          init();
        });
      } else {
        // DOM is ready!
        init();
      }
    }
  };

  function init() {
    if (!initialized) {
      initialized = true;

      if(program_list && program_list.length > 0) {
        let data = [];
        if(drupalSettings.node_type === 'program') {
          data = formatTableDataForProgram();
        }

        if(drupalSettings.node_type === 'customer') {
          data = formatTableDataForCustomer();
        }

        setDatatable('#programsTable', data,10);

      } else if(product_list && product_list.length > 0) {

        if(drupalSettings.node_type === 'product') {
          data = product_list;
          setDatatable('#productsTable', data,5);

        }

      } else {
        $('<div>No data available.</div>').appendTo('#programs_block');
      }

      setPaginatorContent();
      // setDatatable('#productsTable', data, 5);

      $('#ajaxThrobber').hide();
    }
  }

  function setDatatable(id, data, starting) {

    if ( $.fn.dataTable.isDataTable( id ) ) {
      let table = $(id).DataTable();
    } else {

      $(id + ' thead tr').clone(true).appendTo( id + ' thead' );

      let table = $(id).DataTable( {
        data: data,
        deferRender: true,
        paging: true,
        ordering: true,
        orderCellsTop: true,
        pagingType: 'full_numbers',
        lengthMenu: [[starting, 15, 25, 50, 100, -1], [starting, 15, 25, 50, 100, "All"]]
      } );

      $(id + ' thead tr:eq(1) th').each( function (i) {
        let title = $(this).text();
        let option = [];

        option.push('<option value="">Filter '+title+'</option>');
        table.column(i).data().unique().each( function(item) {
          let html = $.parseHTML(item);

          if($(html).find( 'li' ).length > 0) {
            $(html).find( 'li' ).each( function(i, list_item) {
              let list_text = $(list_item).text();
              if(list_text) {
                option.push('<option value="'+list_text+'">'+list_text+'</option>');
              }
            });
          } else {
            let text = $(html).text();
            if(text) {
              option.push('<option value="'+text+'">'+text+'</option>');
            }
          }

        });

        option = $.unique(option.sort());

        let select = $('<select style="width: 100%">'+option.join('')+'</select>');
        $(this).html( select );

        $( 'select', this ).on( 'change', function () {
          if ( table.column(i).search() !== this.value ) {
            table
              .column(i)
              .search( this.value )
              .draw();
          }
        } );
      } );
    }
  }



    // $('#filterCustomers').keyup(delay(function (e) {
    //
    //   try {
    //     let value = $(this).val().toLowerCase();
    //     let temp = list.filter(function(item) {
    //       return item.title.toLowerCase().indexOf(value) > -1;
    //     });
    //
    //     let isset = setPaginator(temp);
    //     if (isset) {
    //       let numberOfPages = Math.ceil( temp.length / 30 );
    //       pagination.simplePaginator('setTotalPages', numberOfPages);
    //       pagination.simplePaginator('changePage', 1);
    //     }
    //
    //
    //   }
    //   catch(err) {
    //     console.log(err);
    //   }
    // }, 500));



  // function delay(callback, ms) {
  //   let timer = 0;
  //   return function() {
  //     let context = this, args = arguments;
  //     clearTimeout(timer);
  //     timer = setTimeout(function () {
  //       callback.apply(context, args);
  //     }, ms || 0);
  //   };
  // }

  function formatTableDataForCustomer() {
    let table = [];

    for (let i = 0; i < program_list.length; i++) {
      let value = program_list[i];

      let prog_row = value.prog_alias ? '<a href="'+ value.prog_alias +'">' + value.prog_title + '</a>' : value.prog_title;
      let here_prod_title_row = value.here_prod_alias ? '<a href="'+ value.here_prod_alias +'">' + value.here_prod_title + '</a>' : value.here_prod_title;
      let customer_product_row = value.cp_alias ? '<a href="'+ value.cp_alias +'">' + value.cp_title + '</a>' : value.cp_title;
      let del_row = value.del_alias ? '<a href="'+ value.del_alias +'">' + (value.del_title ? value.del_title : '') + '</a>' : (value.del_title ? value.del_title : '');
      let system_row = value.sys_title ? value.sys_title : '';
      let nav_row = value.nav_title ? value.nav_title : '';

      let row = [prog_row, here_prod_title_row, customer_product_row, del_row, system_row, nav_row];

      table.push(row);
    }

    return table;
  }

  function formatTableDataForProgram() {
    let table = [];

    for (let i = 0; i < program_list.length; i++) {
      let value = program_list[i];

      let here_prod_title_row = value.here_prod_alias ? '<a href="'+ value.here_prod_alias +'">' + value.here_prod_title + '</a>' : value.here_prod_title;
      let customer_product_row = value.cp_alias ? '<a href="'+ value.cp_alias +'">' + value.cp_title + '</a>' : value.cp_title;
      let customer_row = value.cust_alias ? '<a href="'+ value.cust_alias +'">' + value.cust_title + '</a>' : value.cust_title;
      let del_row = value.del_alias ? '<a href="'+ value.del_alias +'">' + (value.del_title ? value.del_title : '') + '</a>' : (value.del_title ? value.del_title : '');
      let system_row = value.sys_title ? value.sys_title : '';
      let nav_row = value.nav_title ? value.nav_title : '';

      let row = [here_prod_title_row, customer_product_row, del_row, customer_row, system_row, nav_row];

      table.push(row);
    }

    return table;
  }

  function setPaginatorContent() {
    if (list && list.length > 0) {
      $('#paginatorContent').addClass('well');
      setPaginator(list);
    } else {
      $('#paginatorContent').empty().text('No data available.');
    }
  }

  function setPaginator(data) {
    let count = data.length;
    let numberPerPage = 30;
    let numberOfPages = Math.ceil( count / numberPerPage );

    if (pagination) {
      let page = 1;
      let start = (page - 1) * numberPerPage;
      let end = ((page - 1) * numberPerPage) + numberPerPage;
      let pageData = data.slice(start, end);
      let contentDiv = $('<div id="list_container" class="list-group"></div>');
      // pagination.simplePaginator('setTotalPages', numberOfPages);
      // pagination.simplePaginator('changePage', page);

      for (let i = 0; i < pageData.length; i++) {
        let value = pageData[i];

        if(value.alias) {
          $('<a class="list-group-item list-group-item-action" title="'+value.title+'" href="'+value.alias+'">'+ value.title +'</a>').appendTo(contentDiv);
        } else {
          $('<div class="list-group-item list-group-item-action" title="'+value.title+'">'+ value.title +'</div>').appendTo(contentDiv);
        }
      }

      $('#paginatorContent').empty().append(contentDiv);
      return true;
    } else {
      pagination = $('#customer_block_pagination').simplePaginator({
        totalPages: numberOfPages,
        pageChange: function(page) {

          let start = (page - 1) * numberPerPage;
          let end = ((page - 1) * numberPerPage) + numberPerPage;
          let pageData = data.slice(start, end);
          let contentDiv = $('<div id="list_container" class="list-group"></div>');

          for (let i = 0; i < pageData.length; i++) {
            let value = pageData[i];

            if(value.alias) {
              $('<a class="list-group-item list-group-item-action" title="'+value.title+'" href="'+value.alias+'">'+ value.title +'</a>').appendTo(contentDiv);
            } else {
              $('<div class="list-group-item list-group-item-action" title="'+value.title+'">'+ value.title +'</div>').appendTo(contentDiv);
            }
          }

          $('#paginatorContent').empty().append(contentDiv);
          return true;
        }
      });
    }
  }

})(jQuery, Drupal, drupalSettings);


