<?php

namespace Drupal\products_blocks\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;


/**
 * Provides a 'Customers' Block.
 *
 * @Block(
 *   id = "customers_block",
 *   admin_label = @Translation("Customers Block"),
 *   category = @Translation("Customers Block"),
 * )
 */
class CustomersBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */

  public function build() {
    $whole_table = [];
    if (!empty($node = \Drupal::routeMatch()->getParameter('node')) && is_object($node)) {
      $whole_table = $this->getTableData($node->id());
    }

    return [
      '#theme' => 'here_products_customer_template_key',
      '#attached' => [
        'drupalSettings' => [
          'list' => $whole_table
        ],
        'library' => [
          'products_blocks/products-blocks'
        ]
      ]
    ];
  }

  private function getTableData($nid) {

    $query = \ Drupal :: database () -> select ('node_field_data', 'nfd')->distinct();
    $query-> addField ('nfd', 'nid', 'nid');
    $query-> addField ('nfd', 'title', 'title');
    $query-> addField ('url', 'alias', 'alias');

    $query-> leftjoin ('node__field_system_vendor_product_bund', 'bund', 'nfd.nid = bund.entity_id');
    $query-> leftjoin('url_alias', 'url', 'CONCAT( \'/node/\', nfd.nid ) = url.source');

    $query-> leftjoin ('paragraph__field_system_vendor', 'sv', 'bund.field_system_vendor_product_bund_target_id = sv.entity_id');
    $query-> leftjoin('taxonomy_term_field_data', 'tfd_1', 'tfd_1.tid = sv.field_system_vendor_target_id');

    $query-> leftjoin ('paragraph__field_navigation_platform', 'np', 'bund.field_system_vendor_product_bund_target_id = np.entity_id');
    $query-> leftjoin('taxonomy_term_field_data', 'tfd_2', 'tfd_2.tid = np.field_navigation_platform_target_id');

    $query-> leftjoin ('paragraph__field_customer_product', 'cp', 'bund.field_system_vendor_product_bund_target_id = cp.entity_id');
    $query-> leftjoin('node_field_data', 'fd_2', 'cp.field_customer_product_target_id = fd_2.nid');

    $query-> leftjoin ('node__field_delivery_model', 'dm', 'dm.entity_id = cp.field_customer_product_target_id');
    $query-> leftjoin('taxonomy_term_field_data', 'tfd_3', 'tfd_3.tid = dm.field_delivery_model_target_id');

    $query-> leftjoin ('node__field_product_bundle_ids', 'prb', 'prb.entity_id = cp.field_customer_product_target_id');


    $query-> condition ('prb.field_product_bundle_ids_value', "%" . $nid . "%", 'LIKE');
    $query-> condition ('nfd.type', 'customer');
    $query-> orderBy ('nfd.title', 'ASC');

    $customers = $query-> execute() -> fetchAll ();

    return $customers;
  }

//  public function getCacheMaxAge() {
//    return 0;
//  }

  public function getCacheTags() {
    //With this when your node change your block will rebuild
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      //if there is node add its cachetag
      return Cache::mergeTags(parent::getCacheTags(), array('node:' . $node->id()));
    } else {
      //Return default tags instead.
      return parent::getCacheTags();
    }
  }

  public function getCacheContexts() {
    //if you depends on \Drupal::routeMatch()
    //you must set context of this block with 'route' context tag.
    //Every new route this block will rebuild
    return Cache::mergeContexts(parent::getCacheContexts(), array('route'));
  }

}


