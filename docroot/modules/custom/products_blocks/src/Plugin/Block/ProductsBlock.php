<?php

namespace Drupal\products_blocks\Plugin\Block;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Block\BlockBase;
use \ReflectionClass;



/**
 * Provides a 'Products' Block.
 *
 * @Block(
 *   id = "products_block",
 *   admin_label = @Translation("Products Block"),
 *   category = @Translation("Products Block"),
 * )
 */
class ProductsBlock extends BlockBase {

  const CUSTOMER_TOOLTIP = 'Customers that access this product';
  const DELIVERY_MODEL_TOOLTIP = 'Way of delivering HERE data to the customer';
  const SYSTEM_VENDOR_TOOLTIP = 'External supplier how converts HERE data into OEM compilation';
  const NAVIGATION_PLATFORM_TOOLTIP = 'Type of hardware on which the navigation runs';
  const CUSTOMER_PRODUCT_TOOLTIP = 'End product to the customer';
  const HERE_PRODUCT_TOOLTIP = 'Level 2 product HERE data';
  const COMPONENT_TOOLTIP = 'Level 3 product HERE data';
  const PROGRAM_TOOLTIP = 'A group of projects worked upon to create content for a specific product';
  const BUSINESS_TOOLTIP = 'High Level Business groups within HERE';
  const SUB_BUSINESS_TOOLTIP = 'Content Creation Groups within GCO';

  /**
   * {@inheritdoc}
   */

  public function build() {
    $table = 'No data available.';
    $type = '';
    $whole_table = [];
    if (!empty($node = \Drupal::routeMatch()->getParameter('node')) && is_object($node)) {
      $type = $node->getType();
      $whole_table = $this->getTableData($node->id());
      $table = $this->formatTableData($whole_table);
    }

    $reflection = new ReflectionClass(ProductsBlock::class);
    $constants = $reflection->getConstants();

    return [
      '#theme' => 'here_products_template_key',
      '#attached' => [
        'drupalSettings' => [
          'node_type' => $type,
          'product_list' => $table,
          'hover_definitions' => $constants
        ],
        'library' => [
          'products_blocks/products-blocks'
        ]
      ]
    ];
  }

  private function getTableData($nid) {

    $query = \ Drupal :: database () -> select ('node_field_data', 'nfd');
    $query-> addField ('nfd', 'nid', 'nid');
    $query-> addField ('nfd', 'title', 'customer');
    $query-> addField ('url_5', 'alias', 'cust_alias');

    $query-> addField ('tfd_3', 'name', 'delivery_model');
    $query-> addField ('tfd_3', 'tid', 'del_tid');
    $query-> addField ('url_1', 'alias', 'del_alias');

    $query-> addField ('tfd_1', 'name', 'system_vendor');
    $query-> addField ('tfd_1', 'tid', 'sys_tid');
    $query-> addField ('url_2', 'alias', 'sys_alias');


    $query-> addField ('tfd_2', 'name', 'navigation_platform');
    $query-> addField ('tfd_2', 'tid', 'nav_tid');
    $query-> addField ('url_4', 'alias', 'nav_alias');


    $query-> addField ('fd_2', 'title', 'customer_product');
    $query-> addField ('cp', 'field_customer_product_target_id', 'cp_nid');
    $query-> addField ('url_3', 'alias', 'cp_alias');

    $query-> addField ('prb', 'field_product_bundle_ids_value', 'bundle_ids');

    $query-> leftjoin('url_alias', 'url_5', 'CONCAT( \'/node/\', nfd.nid ) = url_5.source');
    $query-> leftjoin ('node__field_system_vendor_product_bund', 'bund', 'nfd.nid = bund.entity_id');

    $query-> leftjoin ('paragraph__field_system_vendor', 'sv', 'bund.field_system_vendor_product_bund_target_id = sv.entity_id');
    $query-> leftjoin('taxonomy_term_field_data', 'tfd_1', 'tfd_1.tid = sv.field_system_vendor_target_id');
    $query-> leftjoin('url_alias', 'url_2', 'CONCAT( \'/taxonomy/term/\', tfd_1.tid ) = url_2.source');

    $query-> leftjoin ('paragraph__field_navigation_platform', 'np', 'bund.field_system_vendor_product_bund_target_id = np.entity_id');
    $query-> leftjoin('taxonomy_term_field_data', 'tfd_2', 'tfd_2.tid = np.field_navigation_platform_target_id');
    $query-> leftjoin('url_alias', 'url_4', 'CONCAT( \'/taxonomy/term/\', tfd_2.tid ) = url_4.source');

    $query-> leftjoin ('paragraph__field_customer_product', 'cp', 'bund.field_system_vendor_product_bund_target_id = cp.entity_id');
    $query-> leftjoin('node_field_data', 'fd_2', 'cp.field_customer_product_target_id = fd_2.nid');
    $query-> leftjoin('url_alias', 'url_3', 'CONCAT( \'/node/\', fd_2.nid ) = url_3.source');

    $query-> leftjoin ('node__field_delivery_model', 'dm', 'dm.entity_id = cp.field_customer_product_target_id');
    $query-> leftjoin('taxonomy_term_field_data', 'tfd_3', 'tfd_3.tid = dm.field_delivery_model_target_id');
    $query-> leftjoin('url_alias', 'url_1', 'CONCAT( \'/taxonomy/term/\', tfd_3.tid ) = url_1.source');

    $query-> leftjoin ('node__field_product_bundle_ids', 'prb', 'prb.entity_id = cp.field_customer_product_target_id');

    $query-> condition ('prb.field_product_bundle_ids_value', "%" . $nid . "%", 'LIKE');
    $query-> condition ('nfd.type', 'customer');
    $query-> orderBy ('fd_2.title', 'ASC');

    $customers = $query-> execute() -> fetchAll ();

    return $customers;
  }

  private function formatTableData($data) {

    $table = [];

    foreach ($data as $row) {
      $bundle_ids = $row->bundle_ids;

      $id_str = substr($bundle_ids, strpos($bundle_ids, ':') + 1);
      $ids = explode(',', $id_str);

      $programs_array = [];
      for ($i = 0; $i < count($ids); $i++) {
        $element = $ids[$i];
        $p = \Drupal\paragraphs\Entity\Paragraph::load( $element );

        if($p && $p->field_product_programs) {
          $tids = $p->field_product_programs->getValue();
          foreach($tids as $tid) {
            array_push($programs_array, $tid['target_id']);
          }
        }
      }

      $programs_array = array_unique($programs_array);
      $program_name = '<ul>';
      foreach($programs_array as $tid) {
        $term = \Drupal\taxonomy\Entity\Term::load($tid);
        if ($term) {
          // Run query
          $alias = $this->getProgramAlias($tid);
          $program_name .= $alias ? '<li><a href="'. $alias .'">' . $term->getName() . '</a></li>' : '<li>' . $term->getName() . '</li>';
        }
      }
      $program_name .= '</ul>';

      $delivery_model_row = $row->del_alias ? '<a href="'. $row->del_alias .'">' . $row->delivery_model . '</a>' : $row->delivery_model;
      $customer_product_row = $row->cp_alias ? '<a href="'. $row->cp_alias .'">' . $row->customer_product . '</a>' : $row->customer_product;
      $customer_row = $row->cust_alias ? '<a href="'. $row->cust_alias .'">' . $row->customer . '</a>' : $row->customer;

      array_push($table, [
        $customer_product_row,
        $delivery_model_row,
        $customer_row,
        $row->system_vendor,
        $row->navigation_platform,
        $program_name
      ]);
    }

    return $table;
  }

  private function getProgramAlias($tid) {
    $query = \ Drupal :: database () -> select ('node_field_data', 'nfd');
    $query-> addField ('url', 'alias', 'alias');
    $query-> join ('node__field_program_name', 'prog', 'nfd.nid = prog.entity_id');
    $query-> leftjoin('url_alias', 'url', 'CONCAT( \'/node/\', nfd.nid ) = url.source');
    $query-> condition ('prog.field_program_name_target_id', $tid);
    $alias = $query-> execute() -> fetchField();

    return $alias ? $alias : NULL;
  }

  public function getCacheTags() {
    //With this when your node change your block will rebuild
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      //if there is node add its cachetag
      return Cache::mergeTags(parent::getCacheTags(), array('node:' . $node->id()));
    } else {
      //Return default tags instead.
      return parent::getCacheTags();
    }
  }

  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), array('route'));
  }

}


