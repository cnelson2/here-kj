<?php

namespace Drupal\products_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use \ReflectionClass;


/**
 * Provides a 'Programs' Block.
 *
 * @Block(
 *   id = "programs_block",
 *   admin_label = @Translation("Programs Block"),
 *   category = @Translation("Programs Block"),
 * )
 */
class ProgramsBlock extends BlockBase {

  const CUSTOMER_TOOLTIP = 'Customers that access this product';
  const DELIVERY_MODEL_TOOLTIP = 'Way of delivering HERE data to the customer';
  const SYSTEM_VENDOR_TOOLTIP = 'External supplier how converts HERE data into OEM compilation';
  const NAVIGATION_PLATFORM_TOOLTIP = 'Type of hardware on which the navigation runs';
  const CUSTOMER_PRODUCT_TOOLTIP = 'End product to the customer';
  const HERE_PRODUCT_TOOLTIP = 'Level 2 product HERE data';
  const COMPONENT_TOOLTIP = 'Level 3 product HERE data';
  const PROGRAM_TOOLTIP = 'A group of projects worked upon to create content for a specific product';
  const BUSINESS_TOOLTIP = 'High Level Business groups within HERE';
  const SUB_BUSINESS_TOOLTIP = 'Content Creation Groups within GCO';

  /**
   * {@inheritdoc}
   */

  public function build() {
    $table = 'No data available.';
    $type = '';
    $whole_table = [];
    if (!empty($node = \Drupal::routeMatch()->getParameter('node')) && is_object($node)) {
      $nid = $node->id();
      $type = $node->getType();

      $whole_table = $this->getTableData($nid, $type);
    }

    $reflection = new ReflectionClass(ProgramsBlock::class);
    $constants = $reflection->getConstants();

    return [
      '#theme' => 'here_programs_template_key',
      '#hover_definitions' => $constants,
      '#node_type' => $type,
      '#attached' => [
        'drupalSettings' => [
          'node_type' => $type,
          'program_list' => $whole_table,
          'hover_definitions' => $constants
        ],
        'library' => [
          'products_blocks/products-blocks'
        ]
      ]
    ];
  }

  private function getTableData($nid, $type) {

    $query = \ Drupal :: database () -> select ('node', 'n');
    $query-> addField ('fd', 'title', 'prog_title');
    $query-> addField ('fd', 'nid', 'prog_nid');
    $query-> addField ('url_7', 'alias', 'prog_alias');

    $query-> addField ('fd_1', 'title', 'here_prod_title');
    $query-> addField ('pname', 'entity_id', 'here_prod_nid');
    $query-> addField ('url', 'alias', 'here_prod_alias');

    $query-> addField ('fd_2', 'title', 'cp_title');
    $query-> addField ('fd_2', 'nid', 'cp_nid');
    $query-> addField ('url_2', 'alias', 'cp_alias');

    $query-> addField ('tfd', 'name', 'del_title');
    $query-> addField ('dm', 'field_delivery_model_target_id', 'del_tid');
    $query-> addField ('url_3', 'alias', 'del_alias');

    $query-> addField ('fd_3', 'title', 'cust_title');
    $query-> addField ('bund', 'entity_id', 'cust_nid');
    $query-> addField ('url_4', 'alias', 'cust_alias');

    $query-> addField ('tfd_2', 'name', 'sys_title');
    $query-> addField ('sys', 'field_system_vendor_target_id', 'sys_tid');
    $query-> addField ('url_5', 'alias', 'sys_alias');

    $query-> addField ('tfd_3', 'name', 'nav_title');
    $query-> addField ('nav', 'field_navigation_platform_target_id', 'nav_tid');
    $query-> addField ('url_6', 'alias', 'nav_alias');


    $query-> leftjoin ('node_field_data', 'fd', 'n.nid = fd.nid');
    $query-> leftjoin ('node__field_program_name', 'prog', 'fd.nid = prog.entity_id');
    $query-> leftjoin ('url_alias', 'url_7', 'CONCAT( \'/node/\', fd.nid ) = url_7.source');


    $query-> leftjoin ('paragraph__field_product_programs', 'para_prog', 'para_prog.field_product_programs_target_id = prog.field_program_name_target_id');
    $query-> leftjoin ('paragraphs_item_field_data', 'pfd', 'para_prog.entity_id = pfd.id');

    $query-> leftjoin ('node_field_data', 'fd_1', 'pfd.parent_id = fd_1.nid');
    $query-> leftjoin ('node__field_product_name', 'pname', 'pfd.parent_id = pname.entity_id');
    $query-> leftjoin ('url_alias', 'url', 'CONCAT( \'/node/\', pname.entity_id ) = url.source');

    $query-> leftjoin ('node__field_referenced_products', 'ref', 'pname.entity_id = ref.field_referenced_products_target_id');
    $query-> leftjoin ('node_field_data', 'fd_2', 'ref.entity_id = fd_2.nid');
    $query-> leftjoin ('url_alias', 'url_2', 'CONCAT( \'/node/\', fd_2.nid ) = url_2.source');

    $query-> leftjoin ('node__field_delivery_model', 'dm', 'fd_2.nid = dm.entity_id');
    $query-> leftjoin ('taxonomy_term_field_data', 'tfd', 'tfd.tid = dm.field_delivery_model_target_id');
    $query-> leftjoin ('url_alias', 'url_3', 'CONCAT( \'/taxonomy/term/\', dm.field_delivery_model_target_id ) = url_3.source');

    $query-> leftjoin ('paragraph__field_customer_product', 'cp', 'cp.field_customer_product_target_id = fd_2.nid');
    $query-> leftjoin ('node__field_system_vendor_product_bund', 'bund', 'bund.field_system_vendor_product_bund_target_id = cp.entity_id');
    $query-> leftjoin ('url_alias', 'url_4', 'CONCAT( \'/node/\', bund.entity_id ) = url_4.source');

    $query-> leftjoin ('node_field_data', 'fd_3', 'bund.entity_id = fd_3.nid');
    $query-> leftjoin ('paragraph__field_system_vendor', 'sys', 'sys.entity_id = cp.entity_id');
    $query-> leftjoin ('url_alias', 'url_5', 'CONCAT( \'/taxonomy/term/\', sys.field_system_vendor_target_id ) = url_5.source');

    $query-> leftjoin ('taxonomy_term_field_data', 'tfd_2', 'tfd_2.tid = sys.field_system_vendor_target_id');
    $query-> leftjoin ('paragraph__field_navigation_platform', 'nav', 'nav.entity_id = cp.entity_id');
    $query-> leftjoin ('url_alias', 'url_6', 'CONCAT( \'/taxonomy/term/\', nav.field_navigation_platform_target_id ) = url_6.source');


    $query-> leftjoin ('taxonomy_term_field_data', 'tfd_3', 'tfd_3.tid = nav.field_navigation_platform_target_id');


    switch ($type) {

      case 'customer':
        $query-> condition ('bund.entity_id', $nid);
        $query-> orderBy ('fd_3.title', 'ASC');
        break;
      case 'program':
        $query-> condition ('fd.nid', $nid);
        $query-> orderBy ('fd_1.title', 'ASC');
        break;
    }

    $customers = $query-> execute() -> fetchAll ();

    return $customers;
  }

  public function getCacheTags() {
    //With this when your node change your block will rebuild
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      //if there is node add its cachetag
      return Cache::mergeTags(parent::getCacheTags(), array('node:' . $node->id()));
    } else {
      //Return default tags instead.
      return parent::getCacheTags();
    }
  }

  public function getCacheContexts() {
    //if you depends on \Drupal::routeMatch()
    //you must set context of this block with 'route' context tag.
    //Every new route this block will rebuild
    return Cache::mergeContexts(parent::getCacheContexts(), array('route'));
  }

}


