# Connect Here KM

Drupal 8 Lightning Distro for Here Technologies

This is a Composer-based installer for the [Lightning](https://www.drupal.org/project/lightning) Drupal distribution.

## Get Started

First, make sure you have a fully functioning LAMP stack on your local machine.
Next, you'll want to clone the repository from HERE's TeamHub [Helix](https://cnelson@hth-ext.it.here.com/COLLAB/projects/km_customer_insight/repositories/git/km_prod_code).
Then, you'll need to create and import the latest database from production.
Finally, you'll need to set up the settings.php file so that the 
database credentials match those of the database you just set up.

Make sure you are able to run `composer` and `drush` from the project root, and `grunt` from the theme root. 

* `composer` is used for dependency management. Normally, Composer will install all dependencies into a `vendor` directory that is *next* to `docroot`, not inside it. Remember to keep the `composer.json` and `composer.lock` files that exist above `docroot` -- they are controlling your dependencies.
* `drush` is used for site maintenance.
* `grunt` is used to compile less files into css.

There are a few commands to get up and going that you'll need to run as well. You'll find them below:

```
$ git clone <HELIX_URL> <WHERE_TO_CLONE>
$ cd km_prod_code
$ composer update drupal/core --with-dependencies
$ drush cr
```


## Maintenance via Composer
Let's build upon this table as a handy cheat sheet for our guide:

| Task                                            | Composer                                          |
|-------------------------------------------------|---------------------------------------------------|
| Installing a contrib project (latest version)   | ```composer require drupal/PROJECT```             |
| Installing a contrib project (specific version) | ```composer require drupal/PROJECT:1.0.0-beta3``` |
| Installing a javascript library (e.g. dropzone) | ```composer require bower-asset/dropzone```       |
| Updating all contrib projects and Drupal core   | ```composer update```                             |
| Updating a single contrib project               | ```composer update drupal/PROJECT```              |
| Updating Drupal core                            | ```composer update drupal/core```                 |

The magic is that Composer, unlike Drush, is a *dependency manager*. If module ```foo version: 1.0.0``` depends on ```baz version: 3.2.0```, Composer will not let you update baz to ```3.3.0``` (or downgrade it to ```3.1.0```, for that matter). Drush has no concept of dependency management. If you've ever accidentally hosed a site because of dependency issues like this, you've probably already realized how valuable Composer can be.

But to be clear: it is still very helpful to use a site management tool like Drush or Drupal Console. Tasks such as database updates (```drush updatedb```) are still firmly in the province of such utilities. This installer will install a copy of Drush (local to the project) in the ```bin``` directory.

### Specifying a version
you can specify a version from the command line with:

    $ composer require drupal/<modulename>:<version> 

For example:

    $ composer require drupal/ctools:3.0.0-alpha26
    $ composer require drupal/token:1.x-dev 

In these examples, the composer version 3.0.0-alpha26 maps to the drupal.org version 8.x-3.0-alpha26 and 1.x-dev maps to 8.x-1.x branch on drupal.org.

If you specify a branch, such as 1.x you must add -dev to the end of the version.

**Composer is only responsible for maintaining the code base**.

## Source Control
If you peek at the ```.gitignore``` we provide, you'll see that certain directories, including all directories containing contributed projects, are excluded from source control. This might be a bit disconcerting if you're newly arrived from Planet Drush, but in a Composer-based project like this one, **you SHOULD NOT commit your installed dependencies to source control**.

When you set up the project, Composer will create a file called ```composer.lock```, which is a list of which dependencies were installed, and in which versions. **Commit ```composer.lock``` to source control!** Then, when your colleagues want to spin up their own copies of the project, all they'll have to do is run ```composer install```, which will install the correct versions of everything in ```composer.lock```.

## Git commands
Let's build upon this table as a handy cheat sheet for our guide:

| Task                                                        | Composer                                          |
|-------------------------------------------------------------|---------------------------------------------------|
| Check the status of your workin directory                   | ```git status```                                  |
| Add all files in working directory to staging area          | ```git add -A```                                  |
| Commit all of the files in the staging area with a detailed message          | ```git commit -m```                               |
| Remove all files from staging area                          | ```git reset```                                   |
| Remove specific file from staging area                      | ```git reset <FILE_NAME>```                       |
| Gives hash number of most recent commit with message and author        | ```git log```                       |
| Shows us a list of all of the current changes in the working directory       | ```git diff```                       |
| Pull all recent changes from the remote repository for that branch        | ```git pull```                       |
| Pushes committed changes to remote repository        | ```git push <REMOTE> <BRANCH>```                       |
| Gives us remote repository information       | ```git remote -v```                       |
| Gives us a list of branches locally and remotely       | ```git branch -a```                       |
